<?php
if(APLICACAO_PRODUCAO == 1){
    echo "<h1> PAGINA DESABILITADA POR MOTIVOS DE SEGURANÇA </h1>";
}else{
    include('Funcoes/ListarProcesso.php');

    $percorrerArquivo = new PercorrerArquivo();

    $percorrerArquivo->percorrer(diretorio);

    $array = RepositorioProcesso::get();
    $modulo = RepositorioProcesso::getModulo();

    ksort($modulo);
    foreach ($array as $key => $valor){
        ksort($array[$key]);
    }

    ?>

    <div class="row">
    
        <!--div id="funcionalidade"-->
        
       
        <div id="funcionalidade" class="col-lg-4">
            <div id="select">

            </div>
            <div id="opcoes">

            </div>
            <div id="esconder_funcionalidades" >
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </div>
        </div>
        <div id="conteudo" class="col-lg-8">
            <div id="mostrar_funcionalidades" class="col-lg-12">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </div>
            <h2> </h2>
            <div>
                <h3> Processo: </h3>

                <form id="form" name="form" method="POST" action="javascript:enviar();"  enctype='multipart/form-data'>


                </form>
            </div>
            <div id="comentario">
                <h3> Comentário: </h3>
                <div>
                    <pre>
                    </pre>
                </div>
            </div>

            <div id="exibicao">
                <h3> Resultado:</h3>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="json-tab" data-toggle="tab" 
                        href="#json_result" role="tab" aria-controls="json_result" aria-selected="true">JSON</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="raw-tab" data-toggle="tab" 
                        href="#raw_result" role="tab" aria-controls="raw_result" aria-selected="false">Raw Data</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="json_result" role="tabpanel" aria-labelledby="json-tab">
                        <div id="resultado" class="code-box">
                            <pre>
                            </pre>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="raw_result" role="tabpanel" aria-labelledby="raw-tab">
                        <div id="resultado_raw" class="code-box">
                        
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
    <script>
    var DGlobal = JSON.parse('<?php echo  json_encode($array) ?>');
    var Modulo = JSON.parse('<?php echo  json_encode($modulo) ?>');

    var processo = '';
    var ModuloAtual = Modulo[0];

    function enviar(){
        var dados = $('#form').serialize();
        console.log(processo,ModuloAtual,processo ,DGlobal[processo].url);
        //$('#resultado pre').html('');
        GCS.conectar('post',
                     '<?php echo PROTOCOLLO ?>://<?php echo URL_PROJETO ?>/api/Teste/'+DGlobal[processo].url+'?ambTeste=1',
                     dados,
                     acao,1,'form');
    }

    function acao(dados){
        console.log(dados);
        try {
            $('#exibicao pre').html(syntaxHighlight(JSON.stringify(JSON.parse(dados), null, 2)));
        } catch (error) {
            $('#exibicao pre').html(dados);
        }
        $('#exibicao #resultado_raw').html(dados);

        $("#resultado").show(0);
        $(".code-box").show(0);

    }

    var conteudo;

    function montar(proc){
        processo = proc;
        conteudo = '';
        $('#conteudo h2').html(proc);
        $('#exibicao pre').html('');
        $('#comentario pre').html(DGlobal[ModuloAtual][processo].comentario);

        $.each(DGlobal[ModuloAtual][processo].campos, function(indice, valor) {
            if(valor['array'] == 1){
                conteudo += '<div>'+valor['nome']+': <input type="type" name="'+valor['nome']+'[]" /> </div>';
            }else if(valor['json'] == 1){
                conteudo += '<div>'+valor['nome']+': <textarea name="'+valor['nome']+'"> </textarea> </div>';
            }else if(String(valor['type']) != '' && String(valor['multiple']) != ''){
                conteudo += '<div>'+valor['nome']+': <input type="'+valor['type']+'" multiple="'+valor['multiple']+'" name="'+valor['nome']+'[]" /> </div>';
            }else if(String(valor['type']) != '')
                conteudo += '<div>'+valor['nome']+': <input type="'+valor['type']+'" name="'+valor['nome']+'" /> </div>';
            else if(valor['chave'] === 'entidade')
                conteudo += '<div>'+valor['nome']+': <input type="hidden" name="'+valor['nome']+'" /> </div>';
            else conteudo += '<div>'+valor['nome']+': <input type="text" name="'+valor['nome']+'" /> </div>';
        });
        conteudo += '<div> <br> <input type="submit" id="test" class="btn btn-primary" ' +
        'value="Execultar" data-toggle="tooltip" data-placement="top" '+
        ' title="IMPORTANTE: Não corrigir a escrita desse botão em hipótese NENHUMA."/> <br><br> </div>';
        $('#form').html(conteudo);
        $("#resultado").hide(0)
    }

    function montarFuncinalidade(){
        var conteudo = '<div><select id="modulo" class="form-control" onchange="javascript:mudarModulo();">';
        /*
        var moduloOrdenado = JSON.parse(JSON.stringify(Modulo)) ;
        for (let i=0; i < moduloOrdenado.length-1; i++) {
            for (let j = i+1; j < moduloOrdenado.length; j++) {
                if (moduloOrdenado[j] < moduloOrdenado[i]) {
                    let aux = moduloOrdenado[i];
                    moduloOrdenado[i] = moduloOrdenado[j];
                    moduloOrdenado[j] = aux;
                }
            }
        }
        */
        $.each(Modulo, function(indice, valor) {
           conteudo += '<option value="'+indice+'" >'+valor+'</option>';
        });
        conteudo += '</select> </div>';
        $('#select').html(conteudo);
        montarOpcoes();
    }

    function  montarOpcoes(){
        var conteudo = '';
         $.each(DGlobal[ModuloAtual], function(indice, valor) {
           conteudo += '<a href="javascript:montar(\''+indice+'\');" >'+indice+'</a> <br/>';
        });
        $('#opcoes').html(conteudo);
    }

    function mudarModulo(){
        ModuloAtual = Modulo[$('#modulo').val()];
         montarOpcoes();
    }

    montarFuncinalidade();

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    function syntaxHighlight(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    $("#esconder_funcionalidades").on('click', function(){
        $("#funcionalidade").hide(0, function(){
            $("#mostrar_funcionalidades").show();
            $("#esconder_funcionalidades").hide()
            $("#conteudo").removeClass("col-lg-8")
            $("#conteudo").addClass("col-lg-12")

        });
    })

    $("#mostrar_funcionalidades").on('click', function(){
        $("#funcionalidade").show(0, function(){
            $("#esconder_funcionalidades").show();
            $("#mostrar_funcionalidades").hide()
            $("#conteudo").removeClass("col-lg-12")
            $("#conteudo").addClass("col-lg-8")
        });
    })
    
    </script>
    <?php
}
