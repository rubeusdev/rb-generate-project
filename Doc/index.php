<!DOCTYPE html>

<html>
    <head>
        <title>Site Rubeus</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/58deae0666.js"></script>
        <!-- Bootstraop -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        
        <script type="text/javascript" src="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/ConexaoServidor/GCS.js"></script>
        <script type="text/javascript" src="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/lib/jquery/jquery.js"></script>
        <script type="text/javascript" src="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/lib/jquery/jquery.form.js"></script>
        <link rel = "stylesheet" type = "text/css" href="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/css/estilo.css"/>
        <link rel = "stylesheet" type = "text/css" href="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/css/normalize.css"/>
        <link rel = "stylesheet" type = "text/css" href="<?php echo DIR_DOC ?>vendor/rubeus/generate-project/Doc/css/estiloBarra.css"/>

        

    </head>
    <body>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
        crossorigin="anonymous"></script>
  
        <!--div id="page"-->
        <div class="container">
            <?php
                $array = array(
                    array("texto" => "Testar", "link" => "?pag=1"),
                    array("texto" => "Upload", "link" => "?pag=2")

                );
            ?>
            <div class="barra-superiror">
                <?php
                    for($i = 0; $i < count($array); $i++){?>
                        <div class="menu"><a href="<?php echo $array[$i]['link'];?>"><?php echo $array[$i]['texto'];?></a></div>
                <?php } ?>
            </div>

            <?php
                switch ($_GET['pag']){
                    case 1:
                        include 'Testar.php';
                        break;
                    /*case 2:
                        include 'Upload.php';
                        break;*/
                    default:
                        include 'Testar.php';
                        break;
                }
            ?>
        </div>
        <script>

        </script>

        
    </body>
</html>
